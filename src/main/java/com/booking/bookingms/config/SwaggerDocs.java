package com.booking.bookingms.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;

public class SwaggerDocs {
    @Bean
    public OpenAPI openApi(){
        return new OpenAPI().info(
                new Info()
                        .title("Hotel Configuration")
                        .version("0.0.1")
                        .description("booking service")
        );
    }
}
