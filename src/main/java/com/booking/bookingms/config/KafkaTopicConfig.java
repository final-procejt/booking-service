package com.booking.bookingms.config;


import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic myTopic1(){
        return TopicBuilder.name("Notification").build();
    }

    @Bean
    public NewTopic myTopic2(){
        return TopicBuilder.name("Hotel").build();
    }
}
