package com.booking.bookingms.exception;



import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GeneralExceptionHandler {


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String,String>> handle(MethodArgumentNotValidException exception){
        Map<String,String> error = new HashMap<>();
        exception.getBindingResult().getFieldErrors().stream().forEach(e->error.put(e.getField(),e.getDefaultMessage()));
        return ResponseEntity.badRequest().body(error);
    }

}
