package com.booking.bookingms.service;

import com.booking.bookingms.model.Booking;
import com.booking.bookingms.model.Token;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TokenService {

    void delete(Token token);

    Optional<List<Token>> findExpiredTokens(LocalDateTime t);

    Booking getBookingByToken(String token);

    void confirmedAt(String token);

    Optional<Token> getToken(String token);
}
