package com.booking.bookingms.service;

import com.booking.bookingms.client.HotelServiceClient;
import com.booking.bookingms.dto.response.*;
import com.booking.bookingms.dto.KafkaDTO;
import com.booking.bookingms.dto.request.BookingReqDTO;
import com.booking.bookingms.util.DtoMapper;
import com.booking.bookingms.model.Booking;
import com.booking.bookingms.model.BookingStatus;
import com.booking.bookingms.model.Token;
import com.booking.bookingms.repository.BookingRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
//EXCEPTION handle OLUNMALIDIR
@Service
@Slf4j
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {
    private final BookingRepository bookingRepository;
    private final TokenServiceImpl tokenServiceImpl;
    private final KafkaTemplate<String,Object> kafkaTemplate;
    private final HotelServiceClient hotelServiceClient;


    @Transactional
    @CircuitBreaker(name = "hotelServiceClient", fallbackMethod = "createBookingFallback")
    public BookingResDTO createBooking(BookingReqDTO bookingReqDTO, UUID userId, String userEmail){


        ResponseEntity<RestResponse<HotelResponseDTO>> hotelResponseDTO
                = hotelServiceClient.getRoomByHotelAndRoomId(bookingReqDTO.getHotelId(),bookingReqDTO.getRoomId());

        RoomResponseDTO responseDTO = hotelResponseDTO.getBody().getData().getRooms().get(0);
        RoomTypeResponseDTO roomTypeResponseDTO = responseDTO.getRoomType();

        int sumOfPeople = bookingReqDTO.getAdults()+bookingReqDTO.getChildren();

        if (sumOfPeople > roomTypeResponseDTO.getRoomCapacity()){
            throw new RuntimeException("cant booked this room for these amount of people");
        }


        if(!isRoomAvailable(bookingReqDTO.getRoomId(),bookingReqDTO.getHotelId(), bookingReqDTO.getCheckIn(),bookingReqDTO.getCheckOut())){
            throw new RuntimeException("Room is not available for the selected dates");
        }

        if (bookingReqDTO.getCheckIn().isEqual(bookingReqDTO.getCheckOut())
                || bookingReqDTO.getCheckIn().isAfter(bookingReqDTO.getCheckOut())){
            throw new RuntimeException("Checkin must not be equal or after checkout");
        }

            //otagin 1 gecesinin qiymeti hesablamaq uchun gunler chixilir
            long daysBetween = ChronoUnit.DAYS.between(bookingReqDTO.getCheckIn(), bookingReqDTO.getCheckOut());
            BigDecimal totalPrice = roomTypeResponseDTO.getRentPerDay().multiply(BigDecimal.valueOf(daysBetween));
            String token = UUID.randomUUID().toString();

            Booking booking = DtoMapper.mapToBooking(bookingReqDTO,userId,BookingStatus.PENDING,totalPrice);
            KafkaDTO kafkaDTO = KafkaDTO.builder()
                .userEmail(userEmail)
                .confirmationCode(token)
                .build();
            Token tokenConfirm = new Token(token, LocalDateTime.now(),LocalDateTime.now().plusMinutes(15),booking);
            kafkaTemplate.send("Notification",kafkaDTO);
            booking.setTokens(List.of(tokenConfirm));
            bookingRepository.save(booking);

            return  DtoMapper.mapToBookingResDto(booking);
    }
    public BookingResDTO createBookingFallback(BookingReqDTO bookingReqDTO, UUID userId, String userEmail, Throwable throwable) {
        throw new RuntimeException("Hotel service is currently unavailable. Please try again later.", throwable);
    }


    //auth olunmush istifadechinin booking tarixchesi gosterilir
    public List<BookingResDTO> getAuthUserAllBookings(UUID userId){
       List<Booking> bookings = bookingRepository
               .findBookingByUserId(userId).orElseThrow(()->new RuntimeException("You have no any bookings"));

      return bookings.stream().map(DtoMapper::mapToBookingResDto).toList();
    }

    public List<BookingResDTO> getAllBookings(){
        List<Booking> bookings = bookingRepository.findAll();

      if (bookings.isEmpty()) {
          throw new RuntimeException("No any bookings");
      }

      return bookings.stream().map(DtoMapper::mapToBookingResDto).toList();
    }

    public Boolean isRoomAvailable(Long roomId,UUID hotelId,LocalDate checkIn, LocalDate checkOut) {
        List<Booking> bookings = bookingRepository
                .findByRoomIdAndHotelIdAndCheckOutAfterAndCheckInBefore(roomId,hotelId,checkIn,checkOut);

        return bookings.isEmpty();
    }

    public Boolean isRoomAvailable(Long roomId,UUID hotelId,LocalDate checkIn, LocalDate checkOut,UUID currentBookingId){
        List<Booking> bookings = bookingRepository
                .findByRoomIdAndHotelIdAndCheckOutAfterAndCheckInBefore(roomId,hotelId,checkIn,checkOut);
       return bookings.stream().noneMatch(booking ->!booking.getId().equals(currentBookingId));
    }




    //Yazilmalidir
    @Transactional
    public BookingResDTO changeBooking(UUID userId, BookingReqDTO bookingReqDTO, UUID bookingId,String userEmail){
        /*feign request -> is room exist or available for certain amount of people? then calculate its price per night */

        ResponseEntity<RestResponse<HotelResponseDTO>> hotelResponseDTO
                = hotelServiceClient.getRoomByHotelAndRoomId(bookingReqDTO.getHotelId(),bookingReqDTO.getRoomId());
        RoomResponseDTO responseDTO = hotelResponseDTO.getBody().getData().getRooms().get(0);
        RoomTypeResponseDTO roomTypeResponseDTO = responseDTO.getRoomType();

        int sumOfPeople = bookingReqDTO.getAdults()+bookingReqDTO.getChildren();

        if (sumOfPeople > roomTypeResponseDTO.getRoomCapacity()){
            throw new RuntimeException("cant booked this room for these amount of people");
        }

        Booking booking = bookingRepository.findBookingByIdAndUserId(bookingId,userId).orElseThrow(
                ()->new RuntimeException("No such booking")
        );

        if (booking.getBookingStatus()==BookingStatus.PENDING){
            throw new RuntimeException("confirm your booking first");
        }

        //otag rezerv olunub olunmuyub?

        if (!isRoomAvailable(bookingReqDTO.getRoomId(),bookingReqDTO.getHotelId(), bookingReqDTO.getCheckIn(), bookingReqDTO.getCheckOut(),bookingId)){

            throw new RuntimeException("Room is not available for the selected dates");
        }

        //otaghin gecesi yeniden hesablanir
        long daysBetween = ChronoUnit.DAYS.between(bookingReqDTO.getCheckIn(), bookingReqDTO.getCheckOut());
        BigDecimal totalPrice = roomTypeResponseDTO.getRentPerDay().multiply(BigDecimal.valueOf(daysBetween));

        booking.setUserId(userId);
        booking.setCheckIn(bookingReqDTO.getCheckIn());
        booking.setCheckOut(bookingReqDTO.getCheckOut());
        booking.setChildren(booking.getChildren());
        booking.setAdults(booking.getAdults());
        booking.setRoomId(bookingReqDTO.getRoomId());
        booking.setHotelId(bookingReqDTO.getHotelId());
        booking.setFinalPrice(totalPrice);



        bookingRepository.save(booking);

        return DtoMapper.mapToBookingResDto(booking);
    }


    @Transactional
    public void cancelBooking(UUID bookingId,UUID userId){
       Booking booking = bookingRepository
               .getBookingByIdAndUserId(bookingId,userId).orElseThrow(()->new RuntimeException("No such booking with associated user"));
       bookingRepository.delete(booking);

    }


    @Transactional
    public BookingResDTO confirmBooking(String token){
        Token token1 = tokenServiceImpl.getToken(token).orElseThrow(()->new RuntimeException("no such token"));

        if (token1.getConfirmedAt()!=null){
            throw new RuntimeException("booking already confirmed");
        }

        if (token1.getExpiresAt().isBefore(LocalDateTime.now())){
            throw new RuntimeException("token expired");
        }

        tokenServiceImpl.confirmedAt(token);

        Booking booking = tokenServiceImpl.getBookingByToken(token);
        booking.setBookingStatus(BookingStatus.COMPLETED);
        bookingRepository.save(booking);
        kafkaTemplate.send("Hotel",booking.getCheckOut());
        return DtoMapper.mapToBookingResDto(booking);
    }

    @Override
    public List<BookingResDTO> getBookingsByHotelId(UUID id) {
       return bookingRepository.findBookingsByHotelId(id).orElseThrow(()->new RuntimeException("No such booking with associated hotel"))
                .stream().map(DtoMapper::mapToBookingResDto).toList();
    }




}
