package com.booking.bookingms.service;

import com.booking.bookingms.dto.response.BookingResDTO;
import com.booking.bookingms.dto.request.BookingReqDTO;
import com.booking.bookingms.model.Booking;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface BookingService {
    BookingResDTO createBooking(BookingReqDTO bookingReqDTO, UUID userId, String userEmail);

    List<BookingResDTO> getAuthUserAllBookings(UUID userId);

    List<BookingResDTO> getAllBookings();

    Boolean isRoomAvailable(Long roomId,UUID hotelId, LocalDate checkIn, LocalDate checkOut);

    BookingResDTO changeBooking(UUID userId, BookingReqDTO bookingReqDTO, UUID bookingId,String userEmail);

    void cancelBooking(UUID bookingId,UUID userId);

    BookingResDTO confirmBooking(String token);

    List<BookingResDTO> getBookingsByHotelId(UUID id);
}
