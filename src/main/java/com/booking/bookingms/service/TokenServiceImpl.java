package com.booking.bookingms.service;

import com.booking.bookingms.model.Booking;
import com.booking.bookingms.model.Token;
import com.booking.bookingms.repository.TokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService{
    private final TokenRepository tokenRepository;

    public void delete(Token token){
        tokenRepository.delete(token);
    }

    public Optional<List<Token>> findExpiredTokens(LocalDateTime t){
        return tokenRepository.findByExpiresAtBefore(t);
    }

    public Booking getBookingByToken(String token){
        Token token1 = getToken(token).orElseThrow();
        return token1.getBooking();
    }

    public void confirmedAt(String token){
    Token token1 = getToken(token).orElseThrow();
    token1.setConfirmedAt(LocalDateTime.now());
    tokenRepository.save(token1);
    }

    public Optional<Token> getToken(String token){
        return tokenRepository.findByToken(token);
    }
}
