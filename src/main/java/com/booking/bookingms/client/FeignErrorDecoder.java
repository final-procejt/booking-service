package com.booking.bookingms.client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class FeignErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        System.out.println("Rsponse: "+response);
        return new RuntimeException("Feign client error: " + response.status());
    }
}
