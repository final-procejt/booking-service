package com.booking.bookingms.client;


import com.booking.bookingms.dto.RoomDTO;
import com.booking.bookingms.dto.request.BookingReqDTO;
import com.booking.bookingms.dto.response.BookingResDTO;
import com.booking.bookingms.dto.response.HotelResponseDTO;
import com.booking.bookingms.dto.response.RestResponse;
import com.booking.bookingms.dto.response.RoomResponseDTO;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@FeignClient(name = "hotel-service", url = "http://localhost:8084", path = "/api/hotels")
public interface HotelServiceClient {

    @GetMapping("/{hotelId}/{roomId}")
    ResponseEntity<RestResponse<HotelResponseDTO>> getRoomByHotelAndRoomId(@PathVariable(value = "hotelId") UUID hotelId,
                                                                           @PathVariable(value = "roomId") Long roomId);
}

//http://192.168.1.65:8084/swagger-ui/index.html#/hotel-controller/getRoomByHotelAndRoomId
//http://192.168.1.65:8084/api/hotels/b939b15d-2c9d-4a6e-8c53-6c3bf319d0e9/1