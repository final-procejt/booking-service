package com.booking.bookingms.repository;

import com.booking.bookingms.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;


import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BookingRepository extends JpaRepository<Booking, UUID> {

    Optional<List<Booking>> findBookingByUserId(UUID id);


    Optional<Booking> getBookingByIdAndUserId(UUID bookingId,UUID userId);

    Optional<Booking> findBookingByIdAndUserId(UUID bookingId,UUID userId);

    Optional<List<Booking>> findBookingsByHotelId(UUID id);

    List<Booking> findByRoomIdAndHotelIdAndCheckOutAfterAndCheckInBefore(Long roomId,UUID hotelId, LocalDate checkIn, LocalDate checkOut);
}
