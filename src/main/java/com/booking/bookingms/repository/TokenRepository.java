package com.booking.bookingms.repository;

import com.booking.bookingms.model.Token;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TokenRepository extends JpaRepository<Token,Long> {
    Optional<Token> findByToken(String token);
    Optional<List<Token>> findByExpiresAtBefore(LocalDateTime time);

}
