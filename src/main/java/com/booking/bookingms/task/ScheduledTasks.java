package com.booking.bookingms.task;

import com.booking.bookingms.model.Token;
import com.booking.bookingms.service.TokenServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ScheduledTasks {
    private final TokenServiceImpl tokenServiceImpl;




    @Scheduled(fixedRate = 60000*15)
    public void deleteUnverifiedTokens(){
        LocalDateTime currentTime = LocalDateTime.now();

        Optional<List<Token>> findUnverifiedTokens = tokenServiceImpl.findExpiredTokens(currentTime);

        findUnverifiedTokens.ifPresent(tokens -> tokens.stream().filter(token -> token.getConfirmedAt() == null)
                .forEach(tokenServiceImpl::delete));


    }
}
