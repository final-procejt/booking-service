package com.booking.bookingms.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;
@Getter
@Setter
@Entity
@Table(name = "token")
@NoArgsConstructor
public class Token {
    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;

    @Column(name = "TOKEN")
    private String token;

    public Token(String token, LocalDateTime createdAt, LocalDateTime expiresAt, Booking booking) {
        this.token = token;
        this.createdAt = createdAt;
        this.expiresAt = expiresAt;
        this.booking = booking;
    }

    @Column(name = "CREATEDAT")
    private LocalDateTime createdAt;
    @Column(name = "EXPIRESAT")
    private LocalDateTime expiresAt;
    @Column(name = "CONFIRMEDAT")
    private LocalDateTime confirmedAt;

    @ManyToOne
    @JoinColumn(name = "booking_token_id")
    private Booking booking;
}
