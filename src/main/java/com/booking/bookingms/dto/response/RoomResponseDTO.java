package com.booking.bookingms.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomResponseDTO {
    String roomPhoto;
    Boolean isBooked;
    RoomTypeResponseDTO roomType;
    LocalDate checkInDate;
    LocalDate checkOutDate;
    UUID hotelId;
    UUID bookingId;
    Double totalPrice;
}
