package com.booking.bookingms.dto.response;

import com.booking.bookingms.model.BookingStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Builder
@Getter
@Setter
public class BookingResDTO {
    private UUID id;
    private Integer adults;
    private Integer children;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private Long roomId;
    private UUID hotelId;
    private UUID userId;
    private BookingStatus status;
    private BigDecimal finalPrice;
}
