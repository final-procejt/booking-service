package com.booking.bookingms.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddressDTO {
    String link;
    String street;
    String city;
    String state;
    Integer pinCode;
    String country;
}
