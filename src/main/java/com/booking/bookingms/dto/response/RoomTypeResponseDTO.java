package com.booking.bookingms.dto.response;

import jakarta.validation.constraints.Min;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class RoomTypeResponseDTO {
    Long id;
    BigDecimal rentPerDay;
    Integer roomCapacity;
    Boolean isActive;
}