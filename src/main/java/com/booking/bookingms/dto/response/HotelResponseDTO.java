package com.booking.bookingms.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class HotelResponseDTO {

    UUID id;
    String name;
    String hotelPhoto;
    String description;
    Integer stars;
    AddressDTO address;
    List<String> hotelAmenities;
    List<RoomResponseDTO> rooms;
    List<ReviewResponseDTO> reviews;
}
