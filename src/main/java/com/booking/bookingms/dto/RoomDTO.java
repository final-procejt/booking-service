package com.booking.bookingms.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class RoomDTO {
    private BigDecimal pricePerNight;
    private Boolean isBooked;
}
