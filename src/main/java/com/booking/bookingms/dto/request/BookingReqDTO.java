package com.booking.bookingms.dto.request;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;


@Data
public class BookingReqDTO {
    @NotNull(message = "Number of adults is required")
    private Integer adults;

    @NotNull(message = "Number of children is required")
    private Integer children;

    @NotNull(message = "Check-in date is required")
    @Future(message = "Check-in date must be in the future")
    private LocalDate checkIn;

    @NotNull(message = "Check-out date is required")
    @Future(message = "Check-out date must be in the future")
    private LocalDate checkOut;

    @NotNull(message = "Room ID is required")
    private Long roomId;

    @NotNull(message = "Hotel ID is required")
    private UUID hotelId;

}
