package com.booking.bookingms.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
public class HotelKafkaDTO {
    private UUID hotelId;
    private Long roomId;
    private LocalDate checkOut;
    private Boolean isBooked;
}
