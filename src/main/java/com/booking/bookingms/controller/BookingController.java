package com.booking.bookingms.controller;

import com.booking.bookingms.dto.response.BookingResDTO;
import com.booking.bookingms.dto.request.BookingReqDTO;
import com.booking.bookingms.service.BookingServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.UUID;

@Validated
@RestController
@RequestMapping("/api/v1/booking")
@RequiredArgsConstructor
@Slf4j
public class BookingController {

    private final BookingServiceImpl bookingServiceImpl;

    @PostMapping("/create")
    public ResponseEntity<BookingResDTO> createBooking(@Valid @RequestBody BookingReqDTO bookingReqDTO,
                                                       @RequestHeader(name = "x-user-email") String userEmail,
                                                       @RequestHeader(name = "x-user-id")UUID userId){
        return ResponseEntity.ok(bookingServiceImpl.createBooking(bookingReqDTO,userId,userEmail));
    }

    //only admin can access to this method
    @GetMapping("/all")
    public ResponseEntity<List<BookingResDTO>> getAllBookings(){
        return ResponseEntity.ok(bookingServiceImpl.getAllBookings());
    }


    //only manager and admin can access to this method
    @GetMapping("/get/{id}")
    public ResponseEntity<List<BookingResDTO>> getBookingsByHotel(@PathVariable UUID id){
        return ResponseEntity.ok(bookingServiceImpl.getBookingsByHotelId(id));
    }

    @GetMapping("/get")
    public ResponseEntity<List<BookingResDTO>> getAuthUserBooking(@RequestHeader(name = "x-user-id")UUID userId){
        return ResponseEntity.ok(bookingServiceImpl.getAuthUserAllBookings(userId));
    }


    //check
    @PutMapping("/update/{id}")
    public ResponseEntity<BookingResDTO> updateBooking(@RequestHeader(name = "x-user-id")UUID userId,
                                                       @RequestHeader(name = "x-user-email") String userEmail,
                                                       @Valid
                                                       @RequestBody BookingReqDTO bookingReqDTO,
                                                       @PathVariable UUID id){
        return ResponseEntity.ok(bookingServiceImpl.changeBooking(userId, bookingReqDTO,id,userEmail));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> cancelBooking(@PathVariable UUID id,@RequestHeader(name = "x-user-id") UUID userId){
        bookingServiceImpl.cancelBooking(id,userId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/confirm")
    public ResponseEntity<BookingResDTO> confirmBooking(@RequestParam String token){
       return ResponseEntity.ok(bookingServiceImpl.confirmBooking(token));
    }
}
