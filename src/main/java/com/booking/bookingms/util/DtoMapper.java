package com.booking.bookingms.util;

import com.booking.bookingms.dto.response.BookingResDTO;
import com.booking.bookingms.dto.request.BookingReqDTO;
import com.booking.bookingms.model.Booking;
import com.booking.bookingms.model.BookingStatus;

import java.math.BigDecimal;
import java.util.UUID;

public class DtoMapper {

    public static BookingResDTO mapToBookingResDto(Booking booking){
        return BookingResDTO.builder()
                .id(booking.getId())
                .finalPrice(booking.getFinalPrice())
                .userId(booking.getUserId())
                .status(booking.getBookingStatus())
                .id(booking.getId())
                .checkIn(booking.getCheckIn())
                .checkOut(booking.getCheckOut())
                .roomId(booking.getRoomId())
                .adults(booking.getAdults())
                .children(booking.getChildren())
                .hotelId(booking.getHotelId())
                .build();
    }

    public static Booking mapToBooking(BookingReqDTO bookingReqDTO){
        return Booking.builder()
                .checkIn(bookingReqDTO.getCheckIn())
                .checkOut(bookingReqDTO.getCheckOut())
                .hotelId(bookingReqDTO.getHotelId())
                .roomId(bookingReqDTO.getRoomId())
                .children(bookingReqDTO.getChildren())
                .adults(bookingReqDTO.getAdults())
                .build();
    }

    public static Booking mapToBooking(BookingReqDTO bookingDTO, UUID userId, BookingStatus bookingStatus, BigDecimal finalPrice){
        return Booking.builder()
                .checkIn(bookingDTO.getCheckIn())
                .userId(userId)
                .finalPrice(finalPrice)
                .bookingStatus(bookingStatus)
                .checkOut(bookingDTO.getCheckOut())
                .hotelId(bookingDTO.getHotelId())
                .roomId(bookingDTO.getRoomId())
                .children(bookingDTO.getChildren())
                .adults(bookingDTO.getAdults())
                .build();
    }

}
