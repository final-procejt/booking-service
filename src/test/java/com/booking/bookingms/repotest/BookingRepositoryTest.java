package com.booking.bookingms.repotest;

import com.booking.bookingms.model.Booking;
import com.booking.bookingms.model.BookingStatus;
import com.booking.bookingms.repository.BookingRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@SpringJUnitConfig
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class BookingRepositoryTest {

    @Autowired
    private BookingRepository bookingRepository;

    private Booking booking;

    private UUID hotelId;
    private UUID userId;
    private UUID bookingId;
    private Long roomId;

    @BeforeEach
    public void setUp() {
        userId = UUID.randomUUID();
        hotelId = UUID.randomUUID();
        roomId = 1L;

        booking = Booking.builder()
                .adults(2)
                .children(2)
                .finalPrice(BigDecimal.valueOf(200.0))
                .checkIn(LocalDate.now().minusDays(1))
                .checkOut(LocalDate.now().plusDays(1))
                .roomId(roomId)
                .hotelId(hotelId)
                .userId(userId)
                .bookingStatus(BookingStatus.COMPLETED)
                .build();

        bookingRepository.save(booking);
        bookingId = booking.getId();
    }

    @Test
    public void testFindBookingByUserId() {
        Optional<List<Booking>> bookings = bookingRepository.findBookingByUserId(userId);

        assertTrue(bookings.isPresent());
        assertEquals(1, bookings.get().size());
    }

    @Test
    public void testGetBookingByIdAndUserId() {
        Optional<Booking> foundBooking = bookingRepository.getBookingByIdAndUserId(bookingId, userId);

        assertTrue(foundBooking.isPresent());
        assertEquals(bookingId, foundBooking.get().getId());
        assertEquals(userId, foundBooking.get().getUserId());
    }

    @Test
    public void testFindBookingByIdAndUserId() {
        Optional<Booking> foundBooking = bookingRepository.findBookingByIdAndUserId(bookingId, userId);

        assertTrue(foundBooking.isPresent());
        assertEquals(bookingId, foundBooking.get().getId());
        assertEquals(userId, foundBooking.get().getUserId());
    }

    @Test
    public void testFindBookingsByHotelId() {
        Optional<List<Booking>> bookings = bookingRepository.findBookingsByHotelId(hotelId);

        assertTrue(bookings.isPresent());
        assertEquals(1, bookings.get().size());
        assertEquals(hotelId, bookings.get().get(0).getHotelId());
    }

    @Test
    public void testFindByRoomIdAndHotelIdAndCheckOutAfterAndCheckInBefore() {
        List<Booking> bookings = bookingRepository.findByRoomIdAndHotelIdAndCheckOutAfterAndCheckInBefore(
                roomId, hotelId, LocalDate.now().minusDays(2), LocalDate.now().plusDays(2));

        assertEquals(1, bookings.size());
        assertEquals(roomId, bookings.get(0).getRoomId());
        assertEquals(hotelId, bookings.get(0).getHotelId());
    }
}
